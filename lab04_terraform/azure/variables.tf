# replace the X with your group ID
variable "groupID" {
  default = "3"   // <========= make change here
}

# ---- DO NOT EDIT anything below this line -----------------------------

variable "lab" {
  default = "4"
}

variable "azure_machine_type" {
  default = "Standard_B1ms"
}

variable "location" {
  default = "westeurope"
}
