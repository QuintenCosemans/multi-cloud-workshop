
# replace the X with your group ID
variable "groupID" {
  default = "3"   // <========= make change here
}

# ---- DO NOT EDIT anything below this line -----------------------------

variable "lab" {
  default = "4"
}

variable "dns_zone_id" {
  default = "Z092701712SREG91O80DN"
}

variable "aws_machine_type" {
  default = "t2.micro"
}

variable "aws_region" {
  default = "eu-west-1"
}

variable "aws_ami" {
  default = "ami-01dd271720c1ba44f" # Ubuntu Server 22.04 LTS (HVM), SSD Volume Type
}
