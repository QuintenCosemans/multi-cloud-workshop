provider "azurerm" {
    features {}
}

terraform {
  backend "http" { // <==== Change this to "http" to run in the GitLab pipeline
  }
}

resource "azurerm_public_ip" "myterraformpublicip" {
    name                         = "terraform-multicloud-ip-group${var.groupID}"
    location                     = var.location
    resource_group_name          = "multicloud-workshop-${var.groupID}"
    allocation_method            = "Static"
}

resource "azurerm_network_security_group" "myterraformnsg" {
    name                = "terraform-multicloud-secgroup-group${var.groupID}"
    location            = var.location
    resource_group_name = "multicloud-workshop-${var.groupID}"
    
    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    
		security_rule {
        name                       = "HTTP"
        priority                   = 1011
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    depends_on = [ azurerm_network_interface.myterraformnic, azurerm_virtual_machine.myterraformvm ]
}

resource "azurerm_network_interface" "myterraformnic" {
    name                = "terraform-multicloud-nic-group${var.groupID}"
    location            = var.location
    resource_group_name = "multicloud-workshop-${var.groupID}"

    ip_configuration {
        name                          = "terraform-multicloud-nicCfg-group${var.groupID}"
        subnet_id                     = "/subscriptions/18c7f371-70f8-4777-9b1b-bcdb0e135c76/resourceGroups/multicloud-workshop/providers/Microsoft.Network/virtualNetworks/multicloud-workshop-vnet/subnets/default"
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.myterraformpublicip.id
    }
}

resource "azurerm_virtual_machine" "myterraformvm" {
    name                  = "terraform-instance-azure-group${var.groupID}"
    location              = var.location
    resource_group_name   = "multicloud-workshop-${var.groupID}"
    network_interface_ids = [azurerm_network_interface.myterraformnic.id]
    vm_size               = var.azure_machine_type

    storage_os_disk {
        name              = "myOsDisk-group${var.groupID}"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }
    delete_os_disk_on_termination = true

    storage_image_reference {
        publisher = "canonical"
        offer     = "0001-com-ubuntu-server-focal"
        sku       = "20_04-lts-gen2"
        version   = "latest"
    }

    os_profile {
        computer_name  = "terraform-instance-azure-group${var.groupID}"
        admin_username = "ubuntu"
        custom_data    = templatefile("cloud-init.yaml",{group_id = "${var.groupID}"})
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/ubuntu/.ssh/authorized_keys"
            key_data = file("~/.ssh/workshop_key.pub")
        }
    }

    tags = {
        group = "${var.groupID}"
        lab = "${var.lab}"
    }

    depends_on = [
      azurerm_network_interface.myterraformnic
    ]
}

resource "azurerm_network_interface_security_group_association" "nicassosiation" {
  network_interface_id      = azurerm_network_interface.myterraformnic.id
  network_security_group_id = azurerm_network_security_group.myterraformnsg.id
}

data "azurerm_public_ip" "test" {
  name                = azurerm_public_ip.myterraformpublicip.name
  resource_group_name = "multicloud-workshop-${var.groupID}"
}

resource "azurerm_dns_a_record" "myterraformdns" {
  name                = "${var.groupID}.tf"
  zone_name           = "workshop.azure.gluo.cloud"
  resource_group_name = "rg-gluosandbox-core"
  ttl                 = 60
  records             = [data.azurerm_public_ip.test.ip_address]
}

output "public_ip_address" {
  value = data.azurerm_public_ip.test.ip_address
}
